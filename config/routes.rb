Rails.application.routes.draw do
  #Rotas para Meal
  get '/meals', to: 'meals#index'
  get '/meals/:id', to: 'meals#show'
  post '/meals', to: 'meals#create'
  put '/meals/:id', to: 'meals#update'
  delete '/meals/:id', to: 'meals#destroy'

  #Rotas para OrderMeal
  get '/order_meals', to: 'order_meals#index'
  get '/order_meals/:id', to: 'order_meals#show'
  post '/order_meals', to: 'order_meals#create'
  put '/order_meals/:id', to: 'order_meals#update'
  delete '/order_meals/:id', to: 'order_meals#destroy'  

  #Rotas para MealCategpries
  get '/meal_categories', to: 'meal_categories#index'
  get '/meal_categories/:id', to: 'meal_categories#show'
  post '/meal_categories', to: 'meal_categories#create'
  put '/meal_categories/:id', to: 'meal_categories#update'
  delete '/meal_categories/:id', to: 'meal_categories#destroy'
  
   #Rotas para Orders
 get '/orders', to: 'order#index'
 get '/orders/:id', to: 'order#show'
 post '/orders', to: 'order#create'
 put '/orders/:id', to: 'order#update'
 delete '/orders/:id ', to: 'order#destroy'
end
