class Meal < ApplicationRecord
   #Active storage
   has_one_attached :photo

   #Relações desta Model
   belongs_to :meal_category
   has_many :order_meals
   #Validações desta Model
   validates :name, presence: true
   validates :description, presence: true
   validates :price, presence: true
   validates :available, presence: true
end
