    class MealCategory < ApplicationRecord
        #Relações desta Model
        has_many :meals
    #Validações desta Model
    validates :name, presence: true
end
