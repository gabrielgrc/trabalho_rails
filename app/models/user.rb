class User < ApplicationRecord
    #Relaçôes com outras models
    has_many :orders
    #Validaçôes desta model
    validates :name, presence: true
    validates :email, presence: true
    validates :password, presence: true
    validates :admin, presence: true
end
