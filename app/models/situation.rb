class Situation < ApplicationRecord
    #Relaçôes com outras models
    has_many :orders
    #Validações desta Model
    validates :description, presence:true
end
