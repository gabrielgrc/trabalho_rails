class MealCategoriesController < ApplicationController

    before_action :set_mealcategory, only: [:show, :update, :destroy]
    def index 
        @mealcategories = MealCategory.all

        render json: @mealcategories
    end

    def show
        render json: @mealcategory
    end
       
    def create
        @mealcategory = MealCategory.new(params[:mealcategory])
        if @mealcategory.save
            render json: @mealcategory, status: :created, location: @mealcategory
        else
            render json: @mealcategory.errors, status: :unprocessable_entity
        end
    end

    def update
        if @mealcategory.update(mealcategory_params)
            render json: @mealcategory
        else
            render json: @mealcategory.errors, status: :unprocessable_entity
        end
    end

    def destroy
        @mealcategory.destroy
    end

    private

    def set_mealcategory
        @mealcategory = MealCategory.find(params[:id])
    end

def mealcategory_params
    params.require(:mealcategory).permit(:name, :description)
end

end
