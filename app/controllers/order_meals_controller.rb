class OrderMealsController < ApplicationController
    
    before_action :set_ordermeal, only: [:show, :destroy, :update]

    def index 
        @ordermeals = OrderMeal.all

        render json: @ordermeals
    end
       
    def create
        @ordermeal = OrderMeal.new(params[:ordermeal])
        
        if @ordermeal.save
            render json: @ordermeal, status: :created, location: @ordermeal
        else
            render json: @ordermeal.errors, status: :unprocessable_entity
        end
    end

    def update
    if @ordermeal.update(ordermeal.params)
        render json: @ordermeal
    else
        render json: @ordermeal.errors, status: :unprocessable_entity
    end
end

    def destroy
        @ordermeal.destroy
    end

    private

    def set_ordermeal
        @ordermeal = OrderMeal.find(params[:id])
    end

    def ordermeal_params
        params.require(:ordermeal).permit(:quantity)
    end
end
