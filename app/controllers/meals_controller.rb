class MealsController < ApplicationController

    before_action :set_meal, only: [:show, :update, :destroy]

    def index 
        @meals = Meal.all

        render json: @meals
    end
       
    def show
        render json: @meal
    end
    
    def create
        @meal = Meal.new(params[:meal])
        if @meal.save
            render json: @meal, status: :created, location: @meal
        else
            render json: @meal.errors, status: :unprocessable_entity
        end
    end  

    def update
        if @meal.update(meal_params)
            render json: @meal
        else
            render json: meal.errors, status: :unprocessable_entity
        end
    end
    
    def destroy
        @meal.destroy
    end

    private

    def set_meal
        @meal = Meal.find(params[:id])
    end

    def meal_params#OBS MUITO IMPORTANTE : AO CRIAR A MODEL MEAL NÃO FOI FEITO A INSERÇÃO DO ATRIBUTO IMAGE POR ESTE NÃO SER TRIVIAL, PORTANTO ESTE PARÂMETRO TERÁ AINDA QUE SER FEITO NA MODEL
        params.require(:meal).permit(:name, :description, :price, :image, :available)
end
    end
